variable "project" {}
variable "zone" {}
variable "region" {
  default = "us-east1"
}
variable "image" {}
variable "identity" {}
variable "name" {}
variable "num_webs" {}
variable "creds" {}